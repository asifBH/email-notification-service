package com.seeka.emailnotificationservice.utils;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.seeka.emailnotificationservice.exception.SeekaException;

@Service
public class SecurityUtils {

	// Not storing it in properties file as it will remain constant and configuration should be same as one in seeka-identity-service
	private static final String encryptionKey = "ASNZBHQWDFVCAWAQ";
	private static final String characterEncoding = "UTF-8";
	private static final String cipherTransformation = "AES/CBC/PKCS5PADDING";
	private static final String aesEncryptionAlgorithem = "AES";
	
	/**
	 * Method For Get Decrypted String which is email Address
	 * 
	 * @param encryptedText
	 * @return decryptedText
	 */
	public String decrypt(String encryptedText) {
		String decryptedText = "";
		try {
			Cipher cipher = Cipher.getInstance(cipherTransformation);
			byte[] key = encryptionKey.getBytes(characterEncoding);
			SecretKeySpec secretKey = new SecretKeySpec(key, aesEncryptionAlgorithem);
			IvParameterSpec ivparameterspec = new IvParameterSpec(key);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, ivparameterspec);
			Base64.Decoder decoder = Base64.getDecoder();
			byte[] cipherText = decoder.decode(encryptedText.getBytes("UTF8"));
			decryptedText = new String(cipher.doFinal(cipherText), "UTF-8");

		} catch (Exception E) {
			throw new SeekaException("SKEN5002", "Exception occured while decrypting email", "Exception occured while decryption email, Please retry or send valid email",
					HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return decryptedText;
	}
	
}
