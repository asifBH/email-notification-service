package com.seeka.emailnotificationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import com.seeka.emailnotificationservice.endpoint.EmailNotification;
import com.seeka.emailnotificationservice.exception.SeekaException;
import com.seeka.emailnotificationservice.processor.EmailProcessor;

@RestController
public class EmailNotificationController implements EmailNotification {

	@Autowired
	private EmailProcessor emailProcessor;

	@Override
	public void sendEmail(String email) {
		try {
			if (StringUtils.isEmpty(email)) {
				throw new SeekaException("SKEN4001", "Email Id not found", "Email ID is Null or Empty",
						HttpStatus.BAD_REQUEST.value());

			}
			emailProcessor.sendEmail(email);
		} catch (Exception e) {
			if (e instanceof SeekaException) {
				throw e;
			} else {
				throw new SeekaException("SKEN5001", e.getMessage() != null ? e.getMessage() : "Internal Server Error",
						e.getCause() != null ? e.getCause().getMessage() : "Internal Server Error",
						HttpStatus.INTERNAL_SERVER_ERROR.value());
			}
		}
	}
}
