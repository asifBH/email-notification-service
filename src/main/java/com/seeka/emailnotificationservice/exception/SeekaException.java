package com.seeka.emailnotificationservice.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class SeekaException extends WebApplicationException {

	private static final long serialVersionUID = 1L;

	/**
	 * Safest flavor of constructor!!!!
	 * 
	 * @param status
	 * @param msg
	 */
	public SeekaException(int status, ErrorWrapper msg) {
		super(Response.status(status).entity(msg).build());
	}

	public SeekaException(String errorCode, String message, String detail, int httpStatusCode) {
		super(Response.status(httpStatusCode).entity(new ErrorWrapper(errorCode, message, detail)).build());
	}

	public ErrorWrapper getExceptionWrapper() {
		Object entity = this.getResponse().getEntity();
		if (ErrorWrapper.class.isAssignableFrom(entity.getClass()))
			return (ErrorWrapper) entity;
		else
			return null;
	}

	public int getHttpStatusCode() {
		return getResponse().getStatus();
	}
}
