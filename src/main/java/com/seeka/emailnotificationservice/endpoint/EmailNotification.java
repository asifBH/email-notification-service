package com.seeka.emailnotificationservice.endpoint;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/seeka/api/v1")
public interface EmailNotification {

	@GET
	@Path("/sendEmail")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void sendEmail(@QueryParam("email") String email);
}
