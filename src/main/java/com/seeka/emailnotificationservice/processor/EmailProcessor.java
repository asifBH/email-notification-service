package com.seeka.emailnotificationservice.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seeka.emailnotificationservice.handler.EmailServiceImpl;
import com.seeka.emailnotificationservice.utils.SecurityUtils;

@Service
public class EmailProcessor {

	@Autowired
	private EmailServiceImpl emailServiceImpl;
	
	@Autowired
	private SecurityUtils securityUtils;
	
	public void sendEmail(String emailAddress) {
		// decrypting excripted email we got in request to get plain text email address. Its not recommended to either store plain text email in DD
		// or send it on network
		String emailAddressPlainText = securityUtils.decrypt(emailAddress);
		// calling method to send email
		emailServiceImpl.sendEmailUsingGmailSmtp(emailAddressPlainText);
	}
}
