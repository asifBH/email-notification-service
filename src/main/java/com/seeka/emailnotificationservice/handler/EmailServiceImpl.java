package com.seeka.emailnotificationservice.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.seeka.emailnotificationservice.exception.SeekaException;

@Component
public class EmailServiceImpl {

	@Autowired
    private JavaMailSender javaMailSender;

	/**
	 * This method will be use for sending email using GMAIL if in future we plan to use multiple SMTP we can create a factory patterns which can
	 * pass us different object for sending email
	 * 
	 * @param emailAddress
	 */
	public void sendEmailUsingGmailSmtp(String emailAddress) {

		try {
			SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
			simpleMailMessage.setTo(emailAddress);
			simpleMailMessage.setSubject("Registration Confirmation Email For Seeka");
			simpleMailMessage.setText("Hello! This is email to notifiy you that you have sucessfully registered in Seeka");
			javaMailSender.send(simpleMailMessage);
		} catch (Exception e) {
			throw new SeekaException("SKEN5003", "Exception occured while sending email", "Exception occured while sending email, Please retry or send valid email address",
					HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}
}
